package com.example.mate.wordapp.charts;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mate.wordapp.R;
import com.github.mikephil.charting.charts.BarChart;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChartTimeDifferenceFragment extends Fragment {


    public ChartTimeDifferenceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chart_time_difference, container, false);
        String language = this.getArguments().getString("languageToRepeat");
        //String language = "fra";
        ChartEntriesMaker chartEntriesMaker = new ChartEntriesMaker();
        BarChart chart = (BarChart) rootView.findViewById(R.id.chart1);
        chartEntriesMaker.differenceBetweenSeries(getContext(), chart, language);
        TextView avgText = (TextView) rootView.findViewById(R.id.avg_textV);
        avgText.setText(chartEntriesMaker.getEntriesAvg());

        return rootView;
    }
}
