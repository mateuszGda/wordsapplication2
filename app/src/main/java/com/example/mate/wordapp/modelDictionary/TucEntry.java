package com.example.mate.wordapp.modelDictionary;


import java.util.List;

public class TucEntry {

    private TucEntryPhrase phrase;
    private List <TucEntryMeanings>  meanings;

    public TucEntryPhrase getPhrase() {
        return phrase;
    }

    public List<TucEntryMeanings> getMeanings() {
        return meanings;
    }




}
