package com.example.mate.wordapp.db;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by użytkownik on 2017-06-24.
 */

public class RepeatHistoryDbQueries {

    private Dao<RepeatHistoryToDatabase, Long> historyDao;
    private List<RepeatHistoryToDatabase> rhtdList;
    private RepeatHistoryToDatabase repeatHistoryToDatabase;

    public RepeatHistoryToDatabase wordsQuery(Context ctx) {
        getDao(ctx);

        try {

            rhtdList = historyDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rhtdList.get(0);
    }

    public List <RepeatHistoryToDatabase> wordRepeatHistoryQuery(Context ctx,Long wordId) {
        getDao(ctx);
        try {
            rhtdList = historyDao.queryBuilder().where().eq("wordId", wordId).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rhtdList;
    }


    public List <RepeatHistoryToDatabase> wordSeriesQuery(Context ctx, String language) {
        getDao(ctx);

        try {
            if (language == null) {
                rhtdList = historyDao.queryBuilder().selectColumns("wordId", "seriesNumber", "currentRepeatDate", "nextRepeatDate").groupBy("wordId").query();
            }else {

                WordDbHelper wordDbHelper = OpenHelperManager.getHelper(ctx, WordDbHelper.class);
                QueryBuilder<WordToDatabase, Long> wordToDatabaseBuilder = wordDbHelper.getWordDao().queryBuilder();
                wordToDatabaseBuilder.where().eq("secondWordLanguage", language);
                rhtdList = historyDao.queryBuilder().selectColumns("wordId", "seriesNumber", "currentRepeatDate", "nextRepeatDate").groupBy("wordId").leftJoin(wordToDatabaseBuilder).query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rhtdList;
    }

    public void addHistoryToDb (Context ctx,WordToDatabase wordId, Long nextRepeatDate, int seriesNumber, int answer){
        RepeatHistoryToDatabase repeatHistoryToDatabase = new RepeatHistoryToDatabase();
        repeatHistoryToDatabase.setAnswer(answer);
        repeatHistoryToDatabase.setSeriesNumber(seriesNumber);
        repeatHistoryToDatabase.setNextRepeatDate(nextRepeatDate);
        repeatHistoryToDatabase.setWordToDatabase(wordId);
        repeatHistoryToDatabase.setCurrentRepeatDate(System.currentTimeMillis());

        getDao(ctx);

        try {
            historyDao.create(repeatHistoryToDatabase);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void getDao(Context ctx) {
        WordDbHelper wordDbHelper = OpenHelperManager.getHelper(ctx, WordDbHelper.class);
        historyDao = wordDbHelper.getHistoryDao();

    }



    public List<RepeatHistoryToDatabase> getRhtdList() {
        return rhtdList;
    }

    public void setRhtdList(List<RepeatHistoryToDatabase> rhtdList) {
        this.rhtdList = rhtdList;
    }
}
