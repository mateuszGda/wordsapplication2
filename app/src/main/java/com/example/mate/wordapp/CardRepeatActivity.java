package com.example.mate.wordapp;

import android.app.Application;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;

import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardRepeatActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener, NavigationView.OnNavigationItemSelectedListener {

    /**
     * A handler object, used for deferring UI operations.
     */
    private Handler mHandler = new Handler();

    /**
     * Whether or not we're showing the back of the card (otherwise showing the front).
     */
    private boolean mShowingBack = false;

    @BindView(R.id.user_navigation_view)
    NavigationView navigationView;
    @BindView(R.id.activity_card_repeat_dl)
    DrawerLayout drawerLayout;
    @BindView(R.id.card_repeat_toolbar)
    Toolbar toolbar;
    @BindView(R.id.card_repeat_fab)
    FloatingActionButton flipButton;
    @BindView(R.id.i_remember_button)
    Button rememberButton;
    @BindView(R.id.i_dont_remember_button)
    Button noRememberButton;
    @BindView(R.id.to_repeat_button)
    Button noProgressButton;
    private boolean isAnswerChecked = false;
    private WordToDatabase wordToDatabase;
    private int repeatScore;
    private static String language;
    private static TextReader textReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_repeat);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();

        language = intent.getStringExtra("languageToRepeat");

        getWordFromDbToRepeat();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        textReader = new TextReader(getApplicationContext());

        if (savedInstanceState == null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new CardFrontFragment())
                    .commit();
        } else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }

        getFragmentManager().addOnBackStackChangedListener(this);

        flipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        rememberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatScore = 2;
                setNextRepeat();
            }
        });

        noRememberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatScore = 0;
                setNextRepeat();
            }
        });

        noProgressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repeatScore = 1;
                setNextRepeat();
            }
        });
    }

    private void getWordFromDbToRepeat() {
        WordDbQueries dq = new WordDbQueries();
        wordToDatabase = dq.wordsToRepeatInLanguageQuery(getApplicationContext(), language);
        if (wordToDatabase.getNextRepeatDate() > System.currentTimeMillis()) {
            finish();
            startActivityForResult(new Intent(getApplicationContext(), MainActivity.class), 1);
        }
    }

    private void setNextRepeat() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
        }
        RepeatLogic repeatLogic = new RepeatLogic(wordToDatabase, repeatScore);
        repeatLogic.rememberFromCardRepeatActivity(getApplicationContext());
        recreate();
    }

    private void checkAnswer(View v) {
        if (!isAnswerChecked) {
            isAnswerChecked = true;
        }
        flipCard();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_card_repeat_dl);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        NavigationManager navigationManager = new NavigationManager();
        navigationManager.chooseItemNavigationView(getApplicationContext(), id);

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void flipCard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }

        mShowingBack = true;

        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out)
                .replace(R.id.container, new CardBackFragment())
                .addToBackStack(null)
                .commit();

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);

        invalidateOptionsMenu();
    }

    /**
     * A fragment representing the front of the card.
     */
    public static class CardFrontFragment extends Fragment {
        public CardFrontFragment() {
        }

        private Context ctx;
        WordToDatabase wordToDatabase;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view1 = inflater.inflate(R.layout.layout_repeat_card_first, container, false);
            ImageButton editWord = (ImageButton) view1.findViewById(R.id.edit_button);
            ImageButton showChart = (ImageButton) view1.findViewById(R.id.chart_button);
            ImageButton speakWord = (ImageButton) view1.findViewById(R.id.loud_speaker_button);

            final TextView question = (TextView) view1.findViewById(R.id.question_part_one_et_card_first);
            WordDbQueries dq = new WordDbQueries();
            wordToDatabase = dq.wordsToRepeatInLanguageQuery(getActivity().getApplication().getApplicationContext(), language);
            question.setText(wordToDatabase.getFirstWord());

            PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(view1.getContext());
            int colourPalettePreference[] = preferencesLanguageManager.getColoursPreferences();
            int colour = colourBackgroundLogic(wordToDatabase.getId(), colourPalettePreference);

            view1.setBackgroundColor(ContextCompat.getColor(getActivity().getApplication().getApplicationContext(), colour));

            editWord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editWord();
                }
            });
            showChart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showChart();
                }
            });


            speakWord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    speak(question.getText().toString(), wordToDatabase.getFirstWordLanguage());
                }
            });
            return view1;
        }

        private void speak(String textToSpeak, String language) {
            textReader.read(language, textToSpeak);
        }

        private void showChart() {
            Intent intent = new Intent(getActivity().getApplication().getApplicationContext(), ChartWordActivity.class);
            intent.putExtra("ID", wordToDatabase.getId());
            intent.putExtra("languageToRepeat", wordToDatabase.getSecondWordLanguage());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().getApplication().getApplicationContext().startActivity(intent);
        }

        private void editWord() {
            Intent intent = new Intent(getActivity().getApplication().getApplicationContext(), EditWordActivity.class);
            intent.putExtra("ID", wordToDatabase.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().getApplication().getApplicationContext().startActivity(intent);
        }

        private int colourBackgroundLogic(Long id, int[] colourPalettePreference) {
            if (id % 4 == 0) {
                return colourPalettePreference[1];
            } else if (id % 3 == 0) {
                return colourPalettePreference[2];
            } else {
                return colourPalettePreference[3];
            }
        }
    }

    public static class CardBackFragment extends Fragment {
        public CardBackFragment() {
        }

        private Context ctx;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.layout_repeat_card_second, container, false);
            final TextView answer = (TextView) view.findViewById(R.id.answer_part_one_et_card_second);
            ImageButton speakWord = (ImageButton) view.findViewById(R.id.loud_speaker_button_card_second);

            WordDbQueries dq = new WordDbQueries();
            final WordToDatabase wordToDatabase = dq.wordsToRepeatInLanguageQuery(getActivity().getApplication().getApplicationContext(), language);
            answer.setText(wordToDatabase.getSecondWord());

            PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(view.getContext());
            int colourPalettePreference[] = preferencesLanguageManager.getColoursPreferences();
            int colour = colourBackgroundLogic(wordToDatabase.getId(), colourPalettePreference);

            view.setBackgroundColor(ContextCompat.getColor(getActivity().getApplication().getApplicationContext(), colour));

            speakWord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    speak(answer.getText().toString(), wordToDatabase.getSecondWordLanguage());
                }
            });
            return view;
        }

        private void speak(String textToSpeak, String language) {
            textReader.read(language, textToSpeak);
        }

        private int colourBackgroundLogic(Long id, int[] colourPalettePreference) {
            if (id % 4 == 0) {
                return colourPalettePreference[1];
            } else if (id % 3 == 0) {
                return colourPalettePreference[2];
            } else {
                return colourPalettePreference[3];
            }
        }
    }
}
