package com.example.mate.wordapp.db;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class WordDbQueries {


    private Dao<WordToDatabase, Long> wordDao;
    private List<WordToDatabase> wtdList;
    private WordToDatabase wordToDatabase;

    public List<WordToDatabase> getWtdList() {
        return wtdList;
    }

    public WordToDatabase wordsQuery(Context ctx) {


        WordDbHelper wdh = OpenHelperManager.getHelper(ctx, WordDbHelper.class);
        try {
            wordDao = wdh.getWordDao();
            wtdList = wordDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return wtdList.get(0);
    }

    public WordToDatabase wordsToRepeatQuery(Context ctx) {

        WordDbHelper wdh = OpenHelperManager.getHelper(ctx, WordDbHelper.class);
        try {
            wordDao = wdh.getWordDao();
            wtdList = wordDao.queryBuilder().limit(1L).orderBy("nextRepeatDate", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wtdList.get(0);
    }

    public WordToDatabase wordsToRepeatInLanguageQuery(Context ctx, String language) {

        WordDbHelper wdh = OpenHelperManager.getHelper(ctx, WordDbHelper.class);
        try {
            wordDao = wdh.getWordDao();
            wtdList = wordDao.queryBuilder().limit(1L).orderBy("nextRepeatDate", true).where().eq("secondWordLanguage",language).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wtdList.get(0);
    }


    public void addWordToDbQuery(String firstLanguage, String wordFirstLanguage, String wordFirstLanguageMeaning,
                                 String secondLanguage, String wordSecondLanguage, String wordSecondLanguageMeaning, Context ctx) {


        getDao(ctx);
        WordToDatabase wordToDatabase = new WordToDatabase();

        wordToDatabase.setFirstWordLanguage(firstLanguage);
        wordToDatabase.setFirstWord(wordFirstLanguage);
        wordToDatabase.setFirstWordMeaning(wordFirstLanguageMeaning);
        wordToDatabase.setSecondWordLanguage(secondLanguage);
        wordToDatabase.setSecondWord(wordSecondLanguage);
        wordToDatabase.setSecondWordMeaning(wordSecondLanguageMeaning);
        wordToDatabase.setSeriesNumber(0);
        wordToDatabase.setLastRepeatDate(System.currentTimeMillis());
        wordToDatabase.setNextRepeatDate(1);
        wordToDatabase.setAddDate(System.currentTimeMillis());


        try {
            wordDao.create(wordToDatabase);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void addExportedWordToDbQuery(String firstLanguage, String wordFirstLanguage, String wordFirstLanguageMeaning,
                                         String secondLanguage, String wordSecondLanguage, String wordSecondLanguageMeaning, int seriesNumber, long lastRepeatDate, long nextRepeatDate, long setAddDate, Context ctx) {


        getDao(ctx);
        WordToDatabase wordToDatabase = new WordToDatabase();

        wordToDatabase.setFirstWordLanguage(firstLanguage);
        wordToDatabase.setFirstWord(wordFirstLanguage);
        wordToDatabase.setFirstWordMeaning(wordFirstLanguageMeaning);
        wordToDatabase.setSecondWordLanguage(secondLanguage);
        wordToDatabase.setSecondWord(wordSecondLanguage);
        wordToDatabase.setSecondWordMeaning(wordSecondLanguageMeaning);
        wordToDatabase.setSeriesNumber(seriesNumber);
        wordToDatabase.setLastRepeatDate(lastRepeatDate);
        wordToDatabase.setNextRepeatDate(nextRepeatDate);
        wordToDatabase.setAddDate(setAddDate);


        try {
            wordDao.create(wordToDatabase);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateWordToDbQuery(Context ctx, WordToDatabase wordToDatabase) {
        WordDbHelper wordDbHelper = OpenHelperManager.getHelper(ctx, WordDbHelper.class);
        wordDao = wordDbHelper.getWordDao();
        try {
            wordDao.update(wordToDatabase);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int countWordsToRepeatQuery(Context ctx) {
        getDao(ctx);
        try {
            return wordDao.queryBuilder().where().lt("nextRepeatDate", System.currentTimeMillis()).query().size();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int countWordsInLanguageToRepeatQuery(Context ctx, String language) {
        getDao(ctx);
        try {
            return wordDao.queryBuilder().where().lt("nextRepeatDate", System.currentTimeMillis()).and().eq("secondWordLanguage",language ).query().size();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void coursesFromDb(Context ctx) {
        getDao(ctx);

        try {
            wtdList = wordDao.queryBuilder().groupBy("secondWordLanguage").query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int countAllWordsInDbQuery(Context ctx) {
        getDao(ctx);
        try {
            return wordDao.queryForAll().size();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int countAllWordsInLanguageInDbQuery(Context ctx, String language) {
        getDao(ctx);
        try {
            return wordDao.queryBuilder().where().eq("secondWordLanguage", language).query().size();
             } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void deleteWordQuery(Context ctx, Long wordId) {
        getDao(ctx);
        try {
            wordDao.deleteById(wordId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public WordToDatabase wordsIdDetails(Context ctx, Long id) {
        getDao(ctx);

        try {
            wtdList = wordDao.queryBuilder().where().eq("id", id).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return wtdList.get(0);
    }

    private void getDao(Context ctx) {
        WordDbHelper wordDbHelper = OpenHelperManager.getHelper(ctx, WordDbHelper.class);
        wordDao = wordDbHelper.getWordDao();
    }

    public List<WordToDatabase> selectDataAddList(Context ctx, String language) {
        getDao(ctx);
        try {
            if (language == null) {
                return wordDao.queryBuilder().selectColumns("addDate").query();
            }else {
                return wordDao.queryBuilder().selectColumns("addDate").where().eq("secondWordLanguage", language).query();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wtdList;
    }

    public void searchWordLike(Context ctx, String wordLike) {
        getDao(ctx);
        try {
            wtdList = wordDao.queryBuilder().where().like("firstWord", '%' + wordLike + '%').or().like("secondWord", '%' + wordLike + '%').query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void searchWordLikeInLanguage(Context ctx, String wordLike, String searchLanguage) {
        getDao(ctx);
        try {
            wtdList = wordDao.queryBuilder().where().like("firstWord", '%' + wordLike + '%').or().like("secondWord", '%' + wordLike + '%').and().eq("secondWordLanguage", searchLanguage).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}