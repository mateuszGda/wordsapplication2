package com.example.mate.wordapp;

/**
 * Created by użytkownik on 2017-03-07.
 */

public class DictionaryWord {
    private String dictionarySecondWord;
    private String dictionarySecondWordMeaning;
    private String dictionaryFirstWordMeaning;
    private String dictionaryFirstWordLanguage;
    private String dictionarySecondWordLanguage;
    private boolean isSelected;

    public DictionaryWord() {
    }

    public DictionaryWord(String dictionarySecondWord, String dictionaryFirstWordMeaning, String dictionarySecondWordMeaning,
                          String dictionaryFirstWordLanguage, String dictionarySecondWordLanguage, boolean isSelected) {
        this.dictionarySecondWord = dictionarySecondWord;
        this.dictionaryFirstWordMeaning = dictionaryFirstWordMeaning;
        this.dictionarySecondWordMeaning = dictionarySecondWordMeaning;
        this.dictionaryFirstWordLanguage = dictionaryFirstWordLanguage;
        this.dictionarySecondWordLanguage = dictionarySecondWordLanguage;
        this.isSelected = isSelected;
    }

    public String getDictionarySecondWord() {
        return dictionarySecondWord;
    }

    public void setDictionarySecondWord(String dictionaryFirstWord) {
        this.dictionarySecondWord = dictionaryFirstWord;
    }

    public String getDictionaryFirstWordMeaning() {
        return dictionaryFirstWordMeaning;
    }

    public void setDictionaryFirstWordMeaning(String dictionaryFirstWordMeaning) {
        this.dictionaryFirstWordMeaning = dictionaryFirstWordMeaning;
    }

    public String getDictionarySecondWordMeaning() {
        return dictionarySecondWordMeaning;
    }

    public void setDictionarySecondWordMeaning(String dictionarySecondWordMeaning) {
        this.dictionarySecondWordMeaning = dictionarySecondWordMeaning;
    }

    public String getDictionaryFirstWordLanguage() {
        return dictionaryFirstWordLanguage;
    }

    public void setDictionaryFirstWordLanguage(String dictionaryFirstWordLanguage) {
        this.dictionaryFirstWordLanguage = dictionaryFirstWordLanguage;
    }

    public String getDictionarySecondWordLanguage() {
        return dictionarySecondWordLanguage;
    }

    public void setDictionarySecondWordLanguage(String dictionarySecondWordLanguage) {
        this.dictionarySecondWordLanguage = dictionarySecondWordLanguage;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }



}
