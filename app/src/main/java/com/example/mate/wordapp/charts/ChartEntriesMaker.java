package com.example.mate.wordapp.charts;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.example.mate.wordapp.CardRepeatActivity;
import com.example.mate.wordapp.ChartWordActivity;
import com.example.mate.wordapp.R;
import com.example.mate.wordapp.db.RepeatHistoryDbQueries;
import com.example.mate.wordapp.db.RepeatHistoryToDatabase;
import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by użytkownik on 2017-06-26.
 */

public class ChartEntriesMaker {


    private String entriesAvg;


    public String getEntriesAvg() {
        return entriesAvg;
    }

    protected void historyOfAddingWordsEntries(Context ctx, BarChart chart, String language) {
        List<BarEntry> entries = new ArrayList<>();

        WordDbQueries wordDbQueries = new WordDbQueries();
        List<WordToDatabase> listOfAddDataWords = wordDbQueries.selectDataAddList(ctx, language);

        int counterWordsAddOneDay = 0;
        int dayOfAdd = 0;
        List<String> labels = new ArrayList<>();

        for (int i = 0; i < listOfAddDataWords.size(); i++) {
            counterWordsAddOneDay = counterWordsAddOneDay + 1;
            DateTime firstDate = new DateTime(Long.valueOf(listOfAddDataWords.get(i).getAddDate()), DateTimeZone.UTC);
            DateTime secondDate = new DateTime();
            DateTime errorTime = new DateTime(Long.valueOf(1), DateTimeZone.UTC);

            if (i < listOfAddDataWords.size() - 1) {
                secondDate = new DateTime(Long.valueOf(listOfAddDataWords.get(i + 1).getAddDate()), DateTimeZone.UTC);
            } else {
                secondDate = secondDate.plusDays(1);
            }

            if (!(firstDate.withTimeAtStartOfDay().isEqual(secondDate.withTimeAtStartOfDay())) || i == listOfAddDataWords.size() - 1) {
                dayOfAdd = dayOfAdd + 1;
                entries.add(new BarEntry(dayOfAdd, counterWordsAddOneDay));
                labels.add(firstDate.withTimeAtStartOfDay().toString());
                counterWordsAddOneDay = 0;

                while ((!firstDate.plusDays(1).withTimeAtStartOfDay().equals(secondDate.withTimeAtStartOfDay()) &&
                        i != listOfAddDataWords.size() - 1) &&
                        !secondDate.withTimeAtStartOfDay().equals(errorTime.withTimeAtStartOfDay()) &&
                        !firstDate.withTimeAtStartOfDay().isEqual(errorTime.withTimeAtStartOfDay())) {
                    dayOfAdd = dayOfAdd + 1;
                    entries.add(new BarEntry(dayOfAdd, counterWordsAddOneDay));
                    firstDate = firstDate.plusDays(1);
                }
            }
        }

        ChartFactory chartFactory = new ChartFactory();
        chartFactory.barChartGenerator(entries, ctx.getString(R.string.words_amount), chart);
    }


    public void wordRepeatHistoryEntries(Context ctx, Long wordId, LineChart chart) {
        List<Entry> entries = new ArrayList<>();
        RepeatHistoryDbQueries repeatHistoryDbQueries = new RepeatHistoryDbQueries();
        List<RepeatHistoryToDatabase> repeatHistoryToDatabaseList = repeatHistoryDbQueries.wordRepeatHistoryQuery(ctx, wordId);
        if (repeatHistoryToDatabaseList.size() > 0) {
            for (int i = 0; i < repeatHistoryToDatabaseList.size(); i++) {
                entries.add(new Entry(repeatHistoryToDatabaseList.get(i).getCurrentRepeatDate(), repeatHistoryToDatabaseList.get(i).getSeriesNumber()));
            }
            ChartFactory chartFactory = new ChartFactory();
            chartFactory.lineChartGenerator(entries, chart);
        }
    }

    protected void lastSeriesOnWord(Context ctx, BarChart chart, String language) {
        List<BarEntry> entries = new ArrayList<>();
        RepeatHistoryDbQueries repeatHistoryDbQueries = new RepeatHistoryDbQueries();
        List<RepeatHistoryToDatabase> repeatHistoryToDatabaseList = repeatHistoryDbQueries.wordSeriesQuery(ctx, language);

        int maxSeria = getMaxSeria(repeatHistoryToDatabaseList);
        double sum = 0;
        double counterSum = 0;

        for (int i = 0; i < maxSeria + 1; i++) {
            int counter = 0;
            for (int j = 0; j < repeatHistoryToDatabaseList.size(); j++) {
                if (repeatHistoryToDatabaseList.get(j).getSeriesNumber() == i) {
                    counter++;
                }
            }
            entries.add(new BarEntry(i, counter));
            sum += i * counter;
            counterSum += counter;
        }
        ChartFactory chartFactory = new ChartFactory();
        chartFactory.barChartGenerator(entries, ctx.getString(R.string.words_amount), chart);
        double avg = sum / counterSum;
        entriesAvg = String.valueOf(sum) + " / " + String.valueOf(counterSum) + " = " + String.valueOf(avg);

    }

    protected void differenceBetweenSeries(Context ctx, BarChart chart, String language) {
        List<BarEntry> entries = new ArrayList<>();
        RepeatHistoryDbQueries repeatHistoryDbQueries = new RepeatHistoryDbQueries();
        List<RepeatHistoryToDatabase> repeatHistoryToDatabaseList = repeatHistoryDbQueries.wordSeriesQuery(ctx, language);
        double sum = 0;
        double counterSum = 0;
        int maxDifference = getMaxTimeDifference(repeatHistoryToDatabaseList);

        for (int i = 0; i < maxDifference + 1; i++) {
            int counter = 0;
            for (int j = 0; j < repeatHistoryToDatabaseList.size(); j++) {
                if (getTimeDifferenceInDays(repeatHistoryToDatabaseList.get(j).getNextRepeatDate(), repeatHistoryToDatabaseList.get(j).getCurrentRepeatDate()) == i) {
                    counter++;
                }
            }
            entries.add(new BarEntry(i, counter));
            sum += i * counter;
            counterSum += counter;
        }
        ChartFactory chartFactory = new ChartFactory();
        chartFactory.barChartGenerator(entries, ctx.getString(R.string.words_amount), chart);
        double avg = sum / counterSum;
        entriesAvg = String.valueOf(sum) + " / " + String.valueOf(counterSum) + " = " + String.valueOf(avg);
    }

    private int getMaxTimeDifference(List<RepeatHistoryToDatabase> repeatHistoryToDatabaseList) {
        int maxDifference = 0;
        for (int i = 0; i < repeatHistoryToDatabaseList.size(); i++) {
            if (maxDifference < getTimeDifferenceInDays(repeatHistoryToDatabaseList.get(i).getNextRepeatDate(), repeatHistoryToDatabaseList.get(i).getCurrentRepeatDate())) {
                maxDifference = getTimeDifferenceInDays(repeatHistoryToDatabaseList.get(i).getNextRepeatDate(), repeatHistoryToDatabaseList.get(i).getCurrentRepeatDate());
            }
        }
        return maxDifference;
    }

    private int getTimeDifferenceInDays(long nextRepeat, long lastRepeat) {
        return Math.round((nextRepeat - lastRepeat) / 86400000);
    }

    private int getMaxSeria(List<RepeatHistoryToDatabase> repeatHistoryToDatabaseList) {
        int maxSeria = 0;
        for (int j = 0; j < repeatHistoryToDatabaseList.size(); j++) {
            if (maxSeria < repeatHistoryToDatabaseList.get(j).getSeriesNumber()) {
                maxSeria = repeatHistoryToDatabaseList.get(j).getSeriesNumber();
            }
        }
        return maxSeria;
    }
}