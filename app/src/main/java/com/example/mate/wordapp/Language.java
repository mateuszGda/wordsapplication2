package com.example.mate.wordapp;

/**
 * Created by użytkownik on 2017-03-07.
 */

public enum Language {

    POL("pol"),
    ENG("eng"),
    FRA("fra"),
    DEU("deu");


    private String language;
    Language(String language){
        this.language = language;
    }

    @Override
    public String toString() {
        return language;
    }
}
