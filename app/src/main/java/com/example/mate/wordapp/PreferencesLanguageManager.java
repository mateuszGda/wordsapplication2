package com.example.mate.wordapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by użytkownik on 2017-03-26.
 */

public class PreferencesLanguageManager {


    public static final String PREFERENCE_NATIVE_LANGUAGE = "key_native_language";
    public static final String PREFERENCE_SECOND_LANGUAGE = "key_second_language";
    public static final String PREFERENCE_COLOUR_PALETTE = "key_colour_palette";
    public static final String PREFERENCE_COLOUR_FIRST = "key_colour_first";
    public static final String PREFERENCE_COLOUR_SECOND = "key_colour_second";
    public static final String PREFERENCE_COLOUR_THIRD = "key_colour_third";
    public static final String PREFERENCES_USER = "user.preferences.file";

    SharedPreferences languagePreferences;

    Context context;

    public PreferencesLanguageManager(Context context) {

        this.context = context;
        languagePreferences = context.getSharedPreferences(PREFERENCES_USER, Context.MODE_PRIVATE);
    }

    protected void savePreferences(String nativeLanguage, String secondLanguage, int[] colourPalette) {

        SharedPreferences.Editor editor = languagePreferences.edit();

        editor.putString(PREFERENCE_NATIVE_LANGUAGE, nativeLanguage);
        editor.putString(PREFERENCE_SECOND_LANGUAGE, secondLanguage);
        editor.putInt(PREFERENCE_COLOUR_PALETTE, colourPalette[0]);
        editor.putInt(PREFERENCE_COLOUR_FIRST, colourPalette[1]);
        editor.putInt(PREFERENCE_COLOUR_SECOND, colourPalette[2]);
        editor.putInt(PREFERENCE_COLOUR_THIRD, colourPalette[3]);

        editor.apply();
    }

    protected void savePreferences(String nativeLanguage, String secondLanguage) {
        int[] nrPalettePreferences = new int[4];
        nrPalettePreferences[0] = 1;
        nrPalettePreferences[1] = R.color.repeatCardPalette2Col1;
        nrPalettePreferences[2] = R.color.repeatCardPalette2Col2;
        nrPalettePreferences[3] = R.color.repeatCardPalette2Col3;
        savePreferences(nativeLanguage, secondLanguage, nrPalettePreferences);

    }

    protected int[] getLanguagePreferences() {

        int[] aLanguagePref = new int[2];

        aLanguagePref[0] = languageNumber(languagePreferences.getString(PREFERENCE_NATIVE_LANGUAGE, Language.POL.toString()));
        aLanguagePref[1] = languageNumber(languagePreferences.getString(PREFERENCE_SECOND_LANGUAGE, Language.POL.toString()));

        return aLanguagePref;
    }

    protected int[] getColoursPreferences() {
        int[] coloursPref = new int[4];
        coloursPref[0] = languagePreferences.getInt(PREFERENCE_COLOUR_PALETTE, 1);
        coloursPref[1] = languagePreferences.getInt(PREFERENCE_COLOUR_FIRST, 1);
        coloursPref[2] = languagePreferences.getInt(PREFERENCE_COLOUR_SECOND, 1);
        coloursPref[3] = languagePreferences.getInt(PREFERENCE_COLOUR_THIRD, 1);
        return coloursPref;
    }

    protected String[] getLanguagePreferencesIso() {

        String[] aLanguagePrefIso = new String[2];

        aLanguagePrefIso[0] = languagePreferences.getString(PREFERENCE_NATIVE_LANGUAGE, Language.POL.toString());
        aLanguagePrefIso[1] = languagePreferences.getString(PREFERENCE_SECOND_LANGUAGE, Language.POL.toString());

        return aLanguagePrefIso;
    }

    private int languageNumber(String language) {
        switch (language) {
            case "pol":
                return 0;
            case "eng":
                return 1;
            case "fra":
                return 2;
            case "deu":
                return 3;
        }
        return 0;
    }

    protected String languageIso(String language) {
        switch (language) {
            case "pol":
                return "pl";
            case "eng":
                return "en";
            case "fra":
                return "fr";
            case "deu":
                return "de";
        }
        return "";
    }
}
