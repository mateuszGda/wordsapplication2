package com.example.mate.wordapp;

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Firma on 29.09.2017.
 */

public class TextReader {
    private TextToSpeech textToSpeech;
    private Context context;

    public TextReader(Context context) {
        textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
            }
        });
        this.context = context;
    }

    protected void read(String language, String textToRead) {
        String languageIso = changeLanguageToIso(language);
        textToSpeech.setLanguage(new Locale(languageIso));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech.speak(textToRead, TextToSpeech.QUEUE_FLUSH, null, "");

        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
            textToSpeech.speak(textToRead, TextToSpeech.QUEUE_FLUSH, map);
        }
    }

    private String changeLanguageToIso(String language) {
        PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(context);
        return preferencesLanguageManager.languageIso(language);
    }
}
