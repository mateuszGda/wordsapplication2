package com.example.mate.wordapp.db;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "repeatHistory")
public class RepeatHistoryToDatabase {


    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField (foreign = true, canBeNull = false, foreignAutoRefresh = true, columnName = "wordId")
    private WordToDatabase wordToDatabase;

    @DatabaseField
    private long currentRepeatDate;
    @DatabaseField
    private long nextRepeatDate;
    @DatabaseField
    private int seriesNumber;
    @DatabaseField
    private int answer;


    public WordToDatabase getWordToDatabase() {
        return wordToDatabase;
    }

    public void setWordToDatabase(WordToDatabase wordToDatabase) {
        this.wordToDatabase = wordToDatabase;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getAnswer() {
        return answer;
    }



    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public long getCurrentRepeatDate() {
        return currentRepeatDate;
    }

    public void setCurrentRepeatDate(long currentRepeatDate) {
        this.currentRepeatDate = currentRepeatDate;
    }

    public long getNextRepeatDate() {
        return nextRepeatDate;
    }

    public void setNextRepeatDate(long nextRepeatDate) {
        this.nextRepeatDate = nextRepeatDate;
    }

    public int getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(int seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    @Override
    public String toString() {
        return "RepeatHistoryToDatabase{" +
                "id=" + id +
                ", wordId=" + wordToDatabase.getId() +
                ", currentRepeatDate=" + currentRepeatDate +
                ", nextRepeatDate=" + nextRepeatDate +
                ", seriesNumber=" + seriesNumber +
                ", answer=" + answer +
                '}';
    }
}