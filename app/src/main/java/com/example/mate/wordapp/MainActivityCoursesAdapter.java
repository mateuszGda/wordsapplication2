package com.example.mate.wordapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;

import java.util.List;

import static java.lang.String.valueOf;

/**
 * Created by Firma on 09.08.2017.
 */

public class MainActivityCoursesAdapter extends RecyclerView.Adapter<MainActivityCoursesAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final Context context;
    private final List<WordToDatabase> listOfWordsInDb;


    public MainActivityCoursesAdapter(Context context, List<WordToDatabase> listOfWordsInDb) {
        this.context = context;
        this.listOfWordsInDb = listOfWordsInDb;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public MainActivityCoursesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.view_holder_courses_list, parent, false);
        return new MainActivityCoursesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainActivityCoursesAdapter.ViewHolder holder, int position) {
        final WordToDatabase wordToDatabase = listOfWordsInDb.get(position);
        final WordDbQueries wordDbQueries = new WordDbQueries();

        holder.secondLanguage.setText(context.getResources().getIdentifier(wordToDatabase.getSecondWordLanguage(), "string", context.getPackageName()));

        holder.wordsInDb.setText(context.getResources().getText(R.string.actually_you_have_in_db) + " " +
                valueOf(wordDbQueries.countAllWordsInLanguageInDbQuery(context, wordToDatabase.getSecondWordLanguage().toString())));

        holder.wordsToRepeat.setText(context.getResources().getText(R.string.actually_you_should_repeat_words) + " " +
                valueOf(wordDbQueries.countWordsInLanguageToRepeatQuery(context, wordToDatabase.getSecondWordLanguage().toString())));


        holder.imageCountryFlag.setImageResource(context.getResources().getIdentifier("ic_flag_of_" + wordToDatabase.getSecondWordLanguage(), "drawable", context.getPackageName()));

        int idImage = context.getResources().getIdentifier(wordToDatabase.getSecondWordLanguage(), "drawable", context.getPackageName());
        holder.imageCountry.setImageResource(idImage);

        holder.repeatButton.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View v) {
                                                       showActivityForLanguage(wordToDatabase, CardRepeatActivity.class);
                                                   }
                                               }
        );
        holder.statisticButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showActivityForLanguage(wordToDatabase, ChartActivity.class);
            }
        });
        holder.wordListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showActivityForLanguage(wordToDatabase, WordListActivity.class);
            }
        });
        holder.learnZoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showActivityForLanguage(wordToDatabase, LanguageActivity.class);
            }
        });
    }

    private void showActivityForLanguage(WordToDatabase wordToDatabase, Class aClass) {
        Intent intent = new Intent(context, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        String language = "languageToRepeat";
        intent.putExtra(language, wordToDatabase.getSecondWordLanguage());
        context.startActivity(intent);

    }


    @Override
    public int getItemCount() {
        return listOfWordsInDb.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView secondLanguage;
        private TextView wordsInDb;
        private TextView wordsToRepeat;
        private Button repeatButton;
        private ImageButton statisticButton;
        private ImageButton wordListButton;
        private ImageButton learnZoneButton;
        private ImageView imageCountry;
        private ImageView imageCountryFlag;

        public ViewHolder(View itemView) {
            super(itemView);
            secondLanguage = (TextView) itemView.findViewById(R.id.secondLanguage);
            wordsInDb = (TextView) itemView.findViewById(R.id.words_in_db);
            wordsToRepeat = (TextView) itemView.findViewById(R.id.words_in_db_to_repeat);
            repeatButton = (Button) itemView.findViewById(R.id.repeat_button);
            statisticButton = (ImageButton) itemView.findViewById(R.id.statistic_button);
            wordListButton = (ImageButton) itemView.findViewById(R.id.edit_word_button);
            learnZoneButton = (ImageButton) itemView.findViewById(R.id.learn_button);
            imageCountry = (ImageView) itemView.findViewById(R.id.second_language_image);
            imageCountryFlag = (ImageView) itemView.findViewById(R.id.flag_image);
        }
    }
}
