package com.example.mate.wordapp.modelDictionary;

/**
 * Created by użytkownik on 2017-03-07.
 */

public class TucEntryMeanings {
    private String language;
    private String text;

    public TucEntryMeanings(String language, String text) {
        this.language = language;
        this.text = text;

    }

    public String getLanguage() {
        return language;
    }

    public String getText() {
        return text;
    }
}
