package com.example.mate.wordapp.retrofitConnection;

import com.example.mate.wordapp.modelDictionary.DictionaryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;



public interface RetrofitApiClient {

    @GET("/gapi/translate")
    Call<DictionaryResponse> getDictionaryCollection(@Query("from") String languageFirst,
                                               @Query("dest") String languageSecond,
                                               @Query("format") String json,
                                               @Query("phrase") String word,
                                               @Query("pretty") String trueWord);
}
