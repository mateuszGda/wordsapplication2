package com.example.mate.wordapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;

import java.util.List;

/**
 * Created by użytkownik on 2017-04-10.
 */

class WordsInDbListAdapter extends RecyclerView.Adapter<WordsInDbListAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView timeToRepeat;
        private TextView firstWord;
        private TextView firstWordMeaning;
        private TextView secondWord;
        private TextView secondWordMeaning;
        private ImageButton deleteButton;
        private ImageButton editButton;
        private ImageButton chartButton;


        public ViewHolder(View itemView) {
            super(itemView);
            timeToRepeat = (TextView) itemView.findViewById(R.id.next_repeat_tv);
            firstWord = (TextView) itemView.findViewById(R.id.first_word_tv);
            secondWord = (TextView) itemView.findViewById(R.id.second_word_tv);
            firstWordMeaning = (TextView) itemView.findViewById(R.id.first_word_meaning_tv);
            secondWordMeaning = (TextView) itemView.findViewById(R.id.second_word_meaning_tv);
            deleteButton = (ImageButton) itemView.findViewById(R.id.delete_button);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_button);
            chartButton = (ImageButton) itemView.findViewById(R.id.chart_button);
        }
    }

    private final LayoutInflater layoutInflater;
    private final Context context;
    private final List<WordToDatabase> listOfWordsInDb;


    public WordsInDbListAdapter(Context context, List listOfWordsInDb) {
        this.context = context;
        this.listOfWordsInDb = listOfWordsInDb;
        layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public WordsInDbListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.view_holder_words_in_db_list, parent, false);

        return new WordsInDbListAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(WordsInDbListAdapter.ViewHolder holder, int position) {

        WordToDatabase wordToDatabase = listOfWordsInDb.get(position);
        final int pos = position;
        final long wordId = wordToDatabase.getId();
        final WordToDatabase wordToDatabaseDetail = listOfWordsInDb.get(position);
        String sTimeToNextRepeat = timeToNextRepeat(wordToDatabase.getNextRepeatDate());

        holder.timeToRepeat.setText(sTimeToNextRepeat);
        holder.firstWord.setText(wordToDatabase.getFirstWord());
        holder.secondWord.setText(wordToDatabase.getSecondWord());
        holder.firstWordMeaning.setText(wordToDatabase.getFirstWordMeaning());
        holder.secondWordMeaning.setText(wordToDatabase.getSecondWordMeaning());
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WordDbQueries wordDbQueries = new WordDbQueries();
                wordDbQueries.deleteWordQuery(context, wordId);
                listOfWordsInDb.remove(pos);
                notifyItemRemoved(pos);
                notifyItemRangeChanged(pos, listOfWordsInDb.size());
            }
        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditWordActivity.class);
                intent.putExtra("ID", wordToDatabaseDetail.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.chartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChartWordActivity.class);
                intent.putExtra("ID",wordToDatabaseDetail.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    private String timeToNextRepeat(Long nextRepeatTime) {
        Long time = nextRepeatTime - System.currentTimeMillis();
        double dTime = time / 1000;
        
        if(dTime < 0){
            return "słowo do powtórzenia";
        }else if (dTime < 60) {
            return " następna powtórka za " +  Math.round(dTime) +  " "+ context.getString(R.string.seconds);
        } else if (dTime < 3600) {
            return " następna powtórka za " +Math.round(dTime /60) +" "+ context.getString(R.string.minuts);
        } else if (dTime < 86400) {
            return " następna powtórka za " +Math.round(dTime / 3600) +" "+ context.getString(R.string.hours);
        } else {
            return " następna powtórka za "+ Math.round(dTime / 86400) +" "+ context.getString(R.string.days);
        }
    }


    @Override
    public int getItemCount() {
        return listOfWordsInDb.size();
    }


}