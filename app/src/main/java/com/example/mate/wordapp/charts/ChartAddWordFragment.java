package com.example.mate.wordapp.charts;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mate.wordapp.R;
import com.github.mikephil.charting.charts.BarChart;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChartAddWordFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_chart_add_word, container, false);

        String language = this.getArguments().getString("languageToRepeat");
        ChartEntriesMaker chartEntriesMaker = new ChartEntriesMaker();
        BarChart chart = (BarChart) rootView.findViewById(R.id.chart1);

        chartEntriesMaker.historyOfAddingWordsEntries(getContext(), chart, language);
        return rootView;

    }

}
