package com.example.mate.wordapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mate.wordapp.db.WordDbQueries;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class AddWordActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.user_navigation_view)
    NavigationView navigationView;
    @BindView(R.id.activity_add_word)
    DrawerLayout drawerLayout;
    @BindView(R.id.add_word_toolbar)
    Toolbar toolbar;
    @BindView(R.id.first_language_add_word_spinner)
    Spinner spinnerFirstLanguage;
    @BindView(R.id.second_language_add_word_spinner)
    Spinner spinnerSecondLanguage;
    @BindView(R.id.add_word_dictionary_fa)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.first_language_word_add_word_et)
    EditText firstLanguageWord;
    @BindView(R.id.second_language_word_add_word_et)
    EditText secondLanguageWord;
    @BindView(R.id.first_language_word_meaning_add_word_et)
    EditText firstLanguageWordMeaning;
    @BindView(R.id.second_language_word_meaning_add_word_et)
    EditText secondLanguageWordMeaning;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ArrayAdapter<Language> languageSpinnerArrAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                Language.values());

        spinnerFirstLanguage.setAdapter(languageSpinnerArrAdapter);
        spinnerSecondLanguage.setAdapter(languageSpinnerArrAdapter);
        loadPreferences();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (firstLanguageWord.getText().toString().trim().equals("")|secondLanguageWord.getText().toString().trim().equals("") ) {
                    Snackbar.make(v,R.string.write_word,Snackbar.LENGTH_SHORT).show();
                }else{
                    addWordToDb(v);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_add_word);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_toolbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        WordDbQueries wordDbQueries = new WordDbQueries();
        int id = item.getItemId();
        if(valueOf(wordDbQueries.countWordsToRepeatQuery(getApplicationContext())).equals("0")&& id == R.id.choose_learning_navigation){
            Toast.makeText(this, R.string.addWord, Toast.LENGTH_SHORT).show();
        }else {
            NavigationManager navigationManager = new NavigationManager();
            navigationManager.chooseItemNavigationView(getApplicationContext(), id);
            finish();

            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void loadPreferences() {
        PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(getBaseContext());
        preferencesLanguageManager.getLanguagePreferences();

        int[] languagePreferences = preferencesLanguageManager.getLanguagePreferences();

        spinnerFirstLanguage.setSelection((languagePreferences[0]));
        spinnerSecondLanguage.setSelection((languagePreferences[1]));
    }

    private void addWordToDb(View v) {
        WordDbQueries wordDbQueries = new WordDbQueries();
        wordDbQueries.addWordToDbQuery(spinnerFirstLanguage.getSelectedItem().toString(),
                firstLanguageWord.getText().toString(), firstLanguageWordMeaning.getText().toString(),
                spinnerSecondLanguage.getSelectedItem().toString(), secondLanguageWord.getText().toString(),
                secondLanguageWordMeaning.getText().toString(), getApplicationContext());
        Snackbar.make(v, R.string.word_is_in_memory, BaseTransientBottomBar.LENGTH_SHORT).show();
        clear();
    }

    private void clear() {
        loadPreferences();
        firstLanguageWord.setText("");
        firstLanguageWordMeaning.setText("");
        secondLanguageWord.setText("");
        secondLanguageWordMeaning.setText("");

    }
}
