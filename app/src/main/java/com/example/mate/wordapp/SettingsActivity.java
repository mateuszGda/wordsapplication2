package com.example.mate.wordapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mate.wordapp.db.WordDbQueries;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class SettingsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    ArrayAdapter<Language> languageSpinnerArrAdapter;

    @BindView(R.id.user_navigation_view)
    NavigationView navigationView;
    @BindView(R.id.activity_settings_dl)
    DrawerLayout drawerLayout;
    @BindView(R.id.settings_toolbar)
    Toolbar toolbar;
    @BindView(R.id.native_language_spinner)
    Spinner spinnerFirstLanguage;
    @BindView(R.id.second_language_spinner)
    Spinner spinnerSecondLanguage;
    @BindView(R.id.save_preferences_button)
    Button saveButton;
    @BindView(R.id.radioButtonColor1)
    RadioButton radioButton1;
    @BindView(R.id.radioButtonColor2)
    RadioButton radioButton2;
    @BindView(R.id.radioButtonColor3)
    RadioButton radioButton3;
    @BindView(R.id.coloursSettingsRadioGroup)
    RadioGroup coloursSettingsRadioGroup;

    private int checkedIdColoursSettings;
    private int[] nrPalettePreferences = new int[4];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        languageSpinnerArrAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                Language.values());

        spinnerFirstLanguage.setAdapter(languageSpinnerArrAdapter);
        spinnerSecondLanguage.setAdapter(languageSpinnerArrAdapter);

        coloursSettingsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                checkedIdColoursSettings = checkedId;
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePreferences(v);
            }
        });
        loadPreferences();
    }

    private void savePreferences(View view) {
        loadColourPalette(checkedIdColoursSettings);
        if (spinnerFirstLanguage.getSelectedItem().toString() == spinnerSecondLanguage.getSelectedItem().toString()) {
            Snackbar.make(view, getResources().getText(R.string.same_language_on_spinners), 2000).show();
        } else {
            PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(getBaseContext());
            preferencesLanguageManager.savePreferences(spinnerFirstLanguage.getSelectedItem().toString(),
                    spinnerSecondLanguage.getSelectedItem().toString(), this.nrPalettePreferences);
            onBackPressed();
        }

    }

    private void loadColourPalette(int checkedIdColoursSettings) {
        switch (checkedIdColoursSettings) {
            case R.id.radioButtonColor1:
                this.nrPalettePreferences[0] = 0;
                this.nrPalettePreferences[1] = R.color.repeatCardPalette1Col1;
                this.nrPalettePreferences[2] = R.color.repeatCardPalette1Col2;
                this.nrPalettePreferences[3] = R.color.repeatCardPalette1Col3;
                break;
            case R.id.radioButtonColor2:
                this.nrPalettePreferences[0] = 1;
                this.nrPalettePreferences[1] = R.color.repeatCardPalette2Col1;
                this.nrPalettePreferences[2] = R.color.repeatCardPalette2Col2;
                this.nrPalettePreferences[3] = R.color.repeatCardPalette2Col3;
                break;
            case R.id.radioButtonColor3:
                this.nrPalettePreferences[0] = 2;
                this.nrPalettePreferences[1] = R.color.repeatCardPalette3Col1;
                this.nrPalettePreferences[2] = R.color.repeatCardPalette3Col2;
                this.nrPalettePreferences[3] = R.color.repeatCardPalette3Col3;
                break;
        }

    }

    private void loadPreferences() {
        PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(getBaseContext());

        int[] nrButton = preferencesLanguageManager.getColoursPreferences();
        int[] languagePreferences = preferencesLanguageManager.getLanguagePreferences();

        spinnerFirstLanguage.setSelection((languagePreferences[0]));
        spinnerSecondLanguage.setSelection((languagePreferences[1]));

        ((RadioButton) coloursSettingsRadioGroup.getChildAt(nrButton[0])).setChecked(true);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_settings_dl);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_toolbar, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        WordDbQueries wordDbQueries = new WordDbQueries();
        int id = item.getItemId();
        if (valueOf(wordDbQueries.countWordsToRepeatQuery(getApplicationContext())).equals("0") && id == R.id.choose_learning_navigation) {
            Toast.makeText(this, R.string.addWord, Toast.LENGTH_SHORT).show();
        } else {
            NavigationManager navigationManager = new NavigationManager();
            navigationManager.chooseItemNavigationView(getApplicationContext(), id);
            finish();

            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }
}
