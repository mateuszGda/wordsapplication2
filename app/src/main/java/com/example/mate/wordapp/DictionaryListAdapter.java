package com.example.mate.wordapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

/**
 * Created by użytkownik on 2017-03-07.
 */


class DictionaryListAdapter extends RecyclerView.Adapter<DictionaryListAdapter.ViewHolder> {


        class ViewHolder extends RecyclerView.ViewHolder {

            TextView textViewWordOnList;
            TextView textViewWordMeaning;
            TextView textViewWordTranslatedMeaning;
            CheckBox cbSelect;

            ViewHolder(View itemView) {
                super(itemView);
                textViewWordOnList = (TextView) itemView.findViewById(R.id.view_holder_word_on_list);
                textViewWordMeaning = (TextView) itemView.findViewById(R.id.view_holder_wordmeaning_on_list);
                textViewWordTranslatedMeaning = (TextView) itemView.findViewById(R.id.view_holder_wordtranslatedmeaning_on_list);
                cbSelect = (CheckBox) itemView.findViewById(R.id.view_holder_checbox);
            }
        }


        private final LayoutInflater layoutInflater;
        private final Context context;
        private final List<DictionaryWord> listToAdd;

        DictionaryListAdapter(Context context, List<DictionaryWord> listToAdd) {
            this.context = context;
            this.listToAdd = listToAdd;
            layoutInflater = LayoutInflater.from(context);


        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.view_holder_dictionary_list, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final int pos = position;
            DictionaryWord word = listToAdd.get(position);
            holder.textViewWordOnList.setText(word.getDictionarySecondWord());

            if (word.getDictionaryFirstWordMeaning() != "") {
                holder.textViewWordMeaning.setText(word.getDictionaryFirstWordMeaning());
            } else {

                StringBuilder stringBuilder =     new StringBuilder()
                        .append(context.getString(R.string.withoutMeaning));


                holder.textViewWordMeaning.setText( stringBuilder);

            }

            if (word.getDictionarySecondWordMeaning() != "") {
                holder.textViewWordTranslatedMeaning.setText(word.getDictionarySecondWordMeaning());
            } else {
                String s = context.getString(R.string.withoutMeaning);
                holder.textViewWordTranslatedMeaning.setText(s);

            }

            holder.cbSelect.setChecked(listToAdd.get(position).isSelected());

            holder.cbSelect.setTag(listToAdd.get(position));


            holder.cbSelect.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    DictionaryWord contact = (DictionaryWord) cb.getTag();

                    contact.setSelected(cb.isChecked());
                    listToAdd.get(pos).setSelected(cb.isChecked());

                }
            });

        }


        @Override
        public int getItemCount() {
            return listToAdd.size();
        }


    }


