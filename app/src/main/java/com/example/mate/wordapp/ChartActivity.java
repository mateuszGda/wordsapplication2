package com.example.mate.wordapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.mate.wordapp.charts.ChartAddWordFragment;
import com.example.mate.wordapp.charts.ChartPagerAdapter;

public class ChartActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        ViewPager viewPager = (ViewPager) findViewById(R.id.vpPager);

        Intent intent =  getIntent();
        String language = intent.getStringExtra("languageToRepeat");




        viewPager.setAdapter(new ChartPagerAdapter(this, getSupportFragmentManager(),language ) {

        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);



    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }
}
