package com.example.mate.wordapp.modelDictionary;

import java.util.List;

/**
 * Created by użytkownik on 2017-03-07.
 */

public class DictionaryResponse {

    private String result;
    private List<TucEntry> tuc;

    public String getResult() {
        return result;
    }

    public List <TucEntry> getTuc() {
        return tuc;
    }
}
