package com.example.mate.wordapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mate.wordapp.db.RepeatHistoryDbQueries;
import com.example.mate.wordapp.db.RepeatHistoryToDatabase;
import com.example.mate.wordapp.db.WordDbHelper;
import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.logger.Log;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class ImportExportActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.activity_import_export)
    DrawerLayout drawerLayout;
    @BindView(R.id.import_export_activity_Toolbar)
    Toolbar toolbar;
    @BindView(R.id.user_navigation_view)
    NavigationView navigationView;
    @BindView(R.id.export_button)
    Button exportButton;
    @BindView(R.id.import_button)
    Button importButton;
    @BindView(R.id.export_history_button)
    Button exportHistoryButton;
    @BindView(R.id.import_exported_words)
    Button importExportedWordsButton;
    private WordDbHelper wordDbHelper;
    private List<WordToDatabase> wordToDatabaseList;
    private List<RepeatHistoryToDatabase> repeatHistoryToDatabaseList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_export);
        ButterKnife.bind(this);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

        exportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importWordsFromDb();
                exportToFile("_words.txt", wordToDatabaseList);
            }
        });

        importButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importWordsToDb();
            }
        });

        exportHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importHistoryFromDb();
                exportToFile("_history.txt", repeatHistoryToDatabaseList);
            }
        });
        importExportedWordsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importExportedWordsToDb();
            }
        });
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_import_export);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        WordDbQueries wordDbQueries = new WordDbQueries();
        int id = item.getItemId();
        if (valueOf(wordDbQueries.countWordsToRepeatQuery(getApplicationContext())).equals("0") && id == R.id.choose_learning_navigation) {
            Toast.makeText(this, R.string.addWord, Toast.LENGTH_SHORT).show();
        } else {
            NavigationManager navigationManager = new NavigationManager();
            navigationManager.chooseItemNavigationView(getApplicationContext(), id);
            finish();

            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void exportToFile(String fileName, List exportedList ) {

        FileManager.instance.openOutputStream(getApplicationContext(), fileName);
        writeInFile(exportedList);
        FileManager.instance.closeOutputStreams();
    }

    private void importWordsFromDb() {
        WordDbQueries wordDbQueries = new WordDbQueries();
        wordDbQueries.wordsQuery(getApplicationContext());
        wordToDatabaseList = wordDbQueries.getWtdList();

    }

    private void writeInFile(List a) {
        for (int i = 0; i < a.size(); i++) {
            String worldToExport = a.get(i).toString();
            FileManager.instance.saveToFileTXT(worldToExport);
        }
    }

    private void importWordsToDb() {
        String textFromFile = FileManager.instance.inputStream(getApplicationContext(), "my_words.txt");

        String[] listOfWords = textFromFile.split("[\\r\\n]+");
        for (int i = 0; i < listOfWords.length; i++) {
            String[] wordToAdd = listOfWords[i].split("\t");
            addWordToDb(wordToAdd[0], wordToAdd[1]);
        }
    }
    private void importExportedWordsToDb(){

        WordDbQueries wordDbQueries = new WordDbQueries();

        String textFromFile = FileManager.instance.inputStream(getApplicationContext(), "words.txt");
        textFromFile = textFromFile.replace("|", ",");
        textFromFile = textFromFile.replace("WordToDatabase{", "|");

        String[] listOfWords = textFromFile.split("\\|");
        for (int i = 1; i < listOfWords.length; i++) {
            String wordToAdd = listOfWords[i];
            wordToAdd = wordToAdd.replace("id=", "|").replace(", firstWord='","|").replace("', firstWordLanguage='","|").replace("', secondWord='","|").replace("', secondWordLanguage='","|").replace("', firstWordMeaning='","|")
                    .replace("', secondWordMeaning='","|").replace("', addDate=","|").replace(", lastRepeatDate=","|").replace(", nextRepeatDate=","|").replace(", seriesNumber=","|").replace("}","|");
            String[] aWordToAdd = wordToAdd.split("\\|");
            String s =  wordToAdd;

            wordDbQueries.addExportedWordToDbQuery(aWordToAdd[3],aWordToAdd[2],aWordToAdd[6],aWordToAdd[5],aWordToAdd[4], aWordToAdd[7],
                    Integer.parseInt(aWordToAdd[11]),Long.parseLong(aWordToAdd[9]),Long.parseLong(aWordToAdd[10]),Long.parseLong(aWordToAdd[8]),getApplicationContext());
        }

    }

    private void addWordToDb(String firstWord, String secondWord) {
       PreferencesLanguageManager preferencesLanguageManager =  new PreferencesLanguageManager(getApplicationContext());
        String[] languagePreferences = preferencesLanguageManager.getLanguagePreferencesIso();
        preferencesLanguageManager.getLanguagePreferencesIso();
        WordDbQueries wordDbQueries = new WordDbQueries();
        wordDbQueries.addWordToDbQuery(languagePreferences[0], firstWord, "", languagePreferences[1], secondWord, "", getApplicationContext());
    }


    private void importHistoryFromDb() {
        RepeatHistoryDbQueries repeatHistoryDbQueries = new RepeatHistoryDbQueries();
        repeatHistoryDbQueries.wordsQuery(getApplicationContext());
        repeatHistoryToDatabaseList = repeatHistoryDbQueries.getRhtdList();
    }
}
