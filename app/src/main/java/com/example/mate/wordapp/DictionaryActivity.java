package com.example.mate.wordapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mate.wordapp.db.WordDbHelper;
import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;
import com.example.mate.wordapp.modelDictionary.TucEntry;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class DictionaryActivity extends AppCompatActivity implements DictionaryAsyncTask.DictionaryCollectionDownloadListener, NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.user_navigation_view)
    NavigationView navigationView;
    @BindView(R.id.search_button)
    ImageButton translateButton;
    @BindView(R.id.search_editText)
    EditText translateEditText;
    @BindView(R.id.recycler_view_list_of_word_dictionary)
    RecyclerView recyclerView;
    @BindView(R.id.activity_dictionary)
    DrawerLayout drawerLayout;
    @BindView(R.id.dictionary_Toolbar)
    Toolbar toolbar;
    @BindView(R.id.add_word_dictionary_fa)
    FloatingActionButton floatingActionButton;
    List<DictionaryWord> wordsListFoundInDictionary;
    @BindView(R.id.dictionary_first_language_spinner)
    Spinner spinnerFirstLanguage;
    @BindView(R.id.dictionary_second_language_spinner)
    Spinner spinnerSecondLanguage;
    @BindView(R.id.dictionary_switch)
    Switch replaceLanguageSwitch;

    private Dao<WordToDatabase, Long> wordDao;
    private WordDbHelper wordDbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        ButterKnife.bind(this);

        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);

        ArrayAdapter<Language> languageSpinnerArrAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                Language.values());

        spinnerFirstLanguage.setAdapter(languageSpinnerArrAdapter);
        spinnerSecondLanguage.setAdapter(languageSpinnerArrAdapter);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        wordDbHelper = OpenHelperManager.getHelper(this, WordDbHelper.class);

        navigationView.setNavigationItemSelectedListener(this);
        loadPreferences();

        translateEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    onClickTranslateWord(v);
                    return true;
                }
                return false;
            }
        });

        translateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(wordsListFoundInDictionary !=null){
                    wordsListFoundInDictionary.clear();
                    loadWordListOnRecycleView();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        translateButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                onClickTranslateWord(view);
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addWordToDb(v);
            }
        });

        replaceLanguageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                replaceLanguageOnSpinners();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_dictionary);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    private void loadPreferences() {
        PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(getBaseContext());
        preferencesLanguageManager.getLanguagePreferences();

        int[] languagePreferences = preferencesLanguageManager.getLanguagePreferences();

        spinnerFirstLanguage.setSelection((languagePreferences[0]));
        spinnerSecondLanguage.setSelection((languagePreferences[1]));
    }

    private void replaceLanguageOnSpinners() {
        int spinnerFirstLanguagePosition = spinnerFirstLanguage.getSelectedItemPosition();
        int spinnerSecondLanguagePosition = spinnerSecondLanguage.getSelectedItemPosition();

        spinnerFirstLanguage.setSelection(spinnerSecondLanguagePosition);
        spinnerSecondLanguage.setSelection(spinnerFirstLanguagePosition);
    }

    private void onClickTranslateWord(View view) {
        if (spinnerFirstLanguage.getSelectedItem().toString() == spinnerSecondLanguage.getSelectedItem().toString()) {
            Snackbar.make(view, getResources().getText(R.string.same_language_on_spinners), 2000).show();

        } else if (translateEditText.getText().toString().equals("")) {
            Snackbar.make(view, getResources().getText(R.string.write_word), 2000).show();
        } else {
            executeAsyncTask(spinnerFirstLanguage.getSelectedItem().toString(), spinnerSecondLanguage.getSelectedItem().toString(),
                    translateEditText.getText().toString().trim());
            view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

        }


    }


    private void executeAsyncTask(String firstLanguage, String secondLanguage, String s) {
        new DictionaryAsyncTask(this).execute(firstLanguage, secondLanguage, "json", s, "true");
    }

    @Override
    public void onDownloaded(List<TucEntry> entries) {
        if (entries.isEmpty()) {

            Snackbar.make(recyclerView, getResources().getText(R.string.word_doesnt_exist_in_dictionary), 2000).show();
            return;
        }

        String languageIso1 = new PreferencesLanguageManager(getBaseContext()).languageIso(spinnerFirstLanguage.getSelectedItem().toString());
        String languageIso2 = new PreferencesLanguageManager(getBaseContext()).languageIso(spinnerSecondLanguage.getSelectedItem().toString());

        String result = "";

        wordsListFoundInDictionary = new ArrayList<>();
        for (int i = 0; i < entries.size(); i++) {


            try {
                if (entries.get(i).getPhrase().getText() != null) {
                    result += entries.get(i).getPhrase().getText();
                    DictionaryWord dw = new DictionaryWord();
                    dw.setDictionarySecondWord((entries.get(i).getPhrase().getText()));
                    dw.setDictionaryFirstWordLanguage(spinnerFirstLanguage.getSelectedItem().toString());
                    dw.setDictionarySecondWordLanguage(spinnerSecondLanguage.getSelectedItem().toString());
                    boolean firstFlag = false;
                    boolean secondFlag = false;
                    for (int j = 0; j < entries.get(i).getMeanings().size(); j++) {


                        if (entries.get(i).getMeanings().get(j).getLanguage().equals(languageIso1) && !firstFlag) {
                            dw.setDictionaryFirstWordMeaning(replaceHtml(entries.get(i).getMeanings().get(j).getText()));
                            firstFlag = true;
                        }
                        if (entries.get(i).getMeanings().get(j).getLanguage().equals(languageIso2) && !secondFlag) {
                            dw.setDictionarySecondWordMeaning(replaceHtml(entries.get(i).getMeanings().get(j).getText()));
                            secondFlag = true;
                        }

                    }
                    if (!firstFlag) {
                        dw.setDictionaryFirstWordMeaning("");
                    }
                    if (!secondFlag) {
                        dw.setDictionarySecondWordMeaning("");
                    }

                    wordsListFoundInDictionary.add(dw);
                }
            } catch (NullPointerException e) {
                e.getStackTrace();
            }
        }


        if (wordsListFoundInDictionary.size() == 0) {
            Snackbar.make(recyclerView, getResources().getText(R.string.word_doesnt_exist_in_dictionary), 2000).show();
            return;
        }

        loadWordListOnRecycleView();


    }

    private void loadWordListOnRecycleView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setAdapter(new DictionaryListAdapter(getBaseContext(), wordsListFoundInDictionary));
    }


    private String replaceHtml(String wordToTranslateMeaning) {
        wordToTranslateMeaning = wordToTranslateMeaning.replace("</i>", "").replace("<i>", "").replace("&quot;", "").replace("&#39", "'");
        return wordToTranslateMeaning;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        WordDbQueries wordDbQueries = new WordDbQueries();
        int id = item.getItemId();
        if (valueOf(wordDbQueries.countWordsToRepeatQuery(getApplicationContext())).equals("0") && id == R.id.choose_learning_navigation) {
            Toast.makeText(this, R.string.addWord, Toast.LENGTH_SHORT).show();
        } else {
            NavigationManager navigationManager = new NavigationManager();
            navigationManager.chooseItemNavigationView(getApplicationContext(), id);
            finish();

            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }


    private void addWordToDb(View view) {
        if (wordsListFoundInDictionary == null) {
            Snackbar.make(view, getResources().getText(R.string.choose_words_to_add), 2000).show();
        } else {
            boolean isSelectedWord = false;
            for (int i = 0; i < wordsListFoundInDictionary.size(); i++) {

                if (wordsListFoundInDictionary.get(i).isSelected()) {
                    isSelectedWord = true;

                    wordDao = wordDbHelper.getWordDao();
                    WordToDatabase wordToDatabase = setWordToDb(i, view);
                    if(wordToDatabase!=null) {

                        try {
                            wordDao.create(wordToDatabase);
                            Snackbar.make(view, getResources().getText(R.string.word_is_in_memory), 2000).show();
                            wordsListFoundInDictionary.remove(i);
                            i = i - 1;
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (!isSelectedWord) {
                Snackbar.make(view, getResources().getText(R.string.choose_words_to_add), 2000).show();
            }
        }
    }

    private WordToDatabase setWordToDb(int i, View view) {
        WordToDatabase wordToDatabase = new WordToDatabase();

        PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(getBaseContext());
        preferencesLanguageManager.getLanguagePreferences();


        String[] languagePreferences = preferencesLanguageManager.getLanguagePreferencesIso();
        String firstWordLanguageInDictionary = spinnerFirstLanguage.getSelectedItem().toString();
        String secondWordLanguageInDictionary = spinnerSecondLanguage.getSelectedItem().toString();
        String firstWordInDictionary = translateEditText.getText().toString();
        String firstWordInDictionaryMeaning;
        String secondWordInDictionary;
        String secondWordInDictionaryMeaning;

        if (wordsListFoundInDictionary.get(i).getDictionaryFirstWordMeaning() != null) {
            firstWordInDictionaryMeaning = wordsListFoundInDictionary.get(i).getDictionaryFirstWordMeaning();
        } else {
            firstWordInDictionaryMeaning = "";
        }

        if (wordsListFoundInDictionary.get(i).getDictionarySecondWord() != null) {
            secondWordInDictionary = wordsListFoundInDictionary.get(i).getDictionarySecondWord();
        } else {
            secondWordInDictionary = "";
        }

        if (wordsListFoundInDictionary.get(i).getDictionarySecondWordMeaning() != null) {
            secondWordInDictionaryMeaning = wordsListFoundInDictionary.get(i).getDictionarySecondWordMeaning();
        } else {
            secondWordInDictionaryMeaning = "";
        }


        if (languagePreferences[0].equals(spinnerFirstLanguage.getSelectedItem().toString())) {

            wordToDatabase.setFirstWordLanguage(firstWordLanguageInDictionary);
            wordToDatabase.setSecondWordLanguage(secondWordLanguageInDictionary);
            wordToDatabase.setFirstWord(firstWordInDictionary);
            wordToDatabase.setFirstWordMeaning(firstWordInDictionaryMeaning);
            wordToDatabase.setSecondWord(secondWordInDictionary);
            wordToDatabase.setSecondWordMeaning(secondWordInDictionaryMeaning);

        } else if (languagePreferences[0].equals(spinnerSecondLanguage.getSelectedItem().toString())) {
            wordToDatabase.setFirstWordLanguage(secondWordLanguageInDictionary);
            wordToDatabase.setSecondWordLanguage(firstWordLanguageInDictionary);
            wordToDatabase.setFirstWord(secondWordInDictionary);
            wordToDatabase.setFirstWordMeaning(secondWordInDictionaryMeaning);
            wordToDatabase.setSecondWord(firstWordInDictionary);
            wordToDatabase.setSecondWordMeaning(firstWordInDictionaryMeaning);
        } else {
            Snackbar.make(view, R.string.changeMainLanguageInPreferences, Snackbar.LENGTH_LONG).show();
            return null;
        }

        wordToDatabase.setAddDate(System.currentTimeMillis());
        wordToDatabase.setSeriesNumber(0);
        wordToDatabase.setLastRepeatDate(System.currentTimeMillis());
        wordToDatabase.setNextRepeatDate(1);
        return wordToDatabase;
    }
}

