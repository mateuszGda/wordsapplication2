package com.example.mate.wordapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditWordActivity extends AppCompatActivity {


    @BindView(R.id.firstWord_edit_word_et)
    EditText firstWord;
    @BindView(R.id.secondWord_edit_word_et)
    EditText secondWord;
    @BindView(R.id.firstWord_meaning_edit_word_et)
    EditText firstWordMeaning;
    @BindView(R.id.secondWord_meaning_edit_word_et)
    EditText secondWordMeaning;
    @BindView(R.id.upload_edit_word_fa)
    FloatingActionButton uploadWordButton;
    @BindView(R.id.edit_word_toolbar)
    Toolbar toolbar;
    private WordToDatabase wordToDatabase;
    private WordDbQueries wordDbQueries;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_word);
        ButterKnife.bind(this);
        setSupportActionBar( toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        long id = getIntent().getLongExtra("ID", 0L);

        wordDbQueries = new WordDbQueries();



        wordToDatabase = wordDbQueries.wordsIdDetails(getApplicationContext(), id);

        firstWord.setText(wordToDatabase.getFirstWord());
        secondWord.setText(wordToDatabase.getSecondWord());
        firstWordMeaning.setText(wordToDatabase.getFirstWordMeaning());
        secondWordMeaning.setText(wordToDatabase.getSecondWordMeaning());
        uploadWordButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                uploadWordToDb(v);

            }
        });



    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return true;
    }

    private void uploadWordToDb(View v) {

        wordToDatabase.setFirstWord(firstWord.getText().toString());
        wordToDatabase.setSecondWord(secondWord.getText().toString());
        wordToDatabase.setFirstWordMeaning(firstWordMeaning.getText().toString());
        wordToDatabase.setSecondWordMeaning(secondWordMeaning.getText().toString());
        wordDbQueries.updateWordToDbQuery(getBaseContext(),wordToDatabase);
        Snackbar.make(v, R.string.word_is_updated,Snackbar.LENGTH_SHORT ).show();



    }
}
