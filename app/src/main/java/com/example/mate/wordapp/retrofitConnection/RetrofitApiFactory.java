package com.example.mate.wordapp.retrofitConnection;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class RetrofitApiFactory {

    public RetrofitApiClient create() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://pl.glosbe.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(RetrofitApiClient.class);
    }
}
