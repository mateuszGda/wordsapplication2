package com.example.mate.wordapp;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * Created by użytkownik on 2017-06-23.
 */

public class FileManager {
    public static final FileManager instance = new FileManager();

    private static final String folder_name = "words";
    private PrintWriter printWriter;
    private String fileName;

    private FileManager() {

    }

    public void openOutputStream(Context ctx, String fileEndName) {
        fileName = String.valueOf(System.currentTimeMillis()) + fileEndName;

        File path = ctx.getExternalFilesDir(null);

        File file = new File(path, fileName);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            printWriter = new PrintWriter(outputStreamWriter);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void saveToFileTXT(String wordToFile) {
        StringBuilder builder = new StringBuilder();
        printWriter.println(wordToFile);
    }

    public void closeOutputStreams() {
        printWriter.close();
    }

    protected String inputStream (Context ctx, String fileName){

        String contents = "";
        File path = ctx.getExternalFilesDir(null);

        File file = new File(path, fileName);

        try {
            int length = (int) file.length();

            byte[] bytes = new byte[length];

            FileInputStream in = new FileInputStream(file);
            try {
                in.read(bytes);
                contents = new String(bytes);
            } finally {

                in.close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contents;
    }


}
