package com.example.mate.wordapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
/**
 * Created by użytkownik on 2017-03-07.
 */


    public class WordDbHelper extends OrmLiteSqliteOpenHelper {
        private static final  String DB_NAME = "words_db.db";
        private static final int DB_VERSION = 4;
        private Dao<WordToDatabase, Long> wordDao;
        private Dao<RepeatHistoryToDatabase, Long> historyDao;



        public WordDbHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
            try {
                TableUtils.createTable(connectionSource,WordToDatabase.class);
                TableUtils.createTable(connectionSource,RepeatHistoryToDatabase.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
            try {
                TableUtils.dropTable(connectionSource, WordToDatabase.class,true);
                TableUtils.dropTable(connectionSource, RepeatHistoryToDatabase.class,true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            onCreate(database,connectionSource);
        }

        public Dao<WordToDatabase, Long> getWordDao() {
            if (wordDao == null) {

                try {
                    wordDao = getDao(WordToDatabase.class);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return wordDao;
        }

        public Dao<RepeatHistoryToDatabase,Long>getHistoryDao() {
            if (historyDao == null) {
                try {
                    historyDao = getDao(RepeatHistoryToDatabase.class);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return historyDao;
        }


}

