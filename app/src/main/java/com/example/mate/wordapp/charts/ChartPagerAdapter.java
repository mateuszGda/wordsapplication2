package com.example.mate.wordapp.charts;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.mate.wordapp.R;

/**
 * Created by Firma on 29.07.2017.
 */

public class ChartPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private String language;

    public ChartPagerAdapter(Context context, FragmentManager fm, String language) {
        super(fm);
        this.context =context;
        this.language = language;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString("languageToRepeat", language);

        switch (position) {
            case 0:
                ChartAddWordFragment chartAddWordFragment = new ChartAddWordFragment();
                chartAddWordFragment.setArguments(bundle);
                return chartAddWordFragment;
            case 1:
                ChartWordSeriesFragment chartWordSeriesFragment = new ChartWordSeriesFragment();
                chartWordSeriesFragment.setArguments(bundle);
                return chartWordSeriesFragment;
            case 2:
                ChartTimeDifferenceFragment chartTimeDifferenceFragment = new ChartTimeDifferenceFragment();
                chartTimeDifferenceFragment.setArguments(bundle);
                return chartTimeDifferenceFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }


    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.add);
            case 1:
                return context.getString(R.string.series);
            case 2:
                return context.getString(R.string.difference);
        }
        return null;
    }
}
