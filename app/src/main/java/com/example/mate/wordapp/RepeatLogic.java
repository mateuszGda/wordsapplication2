package com.example.mate.wordapp;

import android.content.Context;

import com.example.mate.wordapp.db.RepeatHistoryDbQueries;
import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;
import com.j256.ormlite.dao.Dao;

/**
 * Created by użytkownik on 2017-04-01.
 */

public class RepeatLogic {
    private WordToDatabase wordToDatabase;
    private Dao<WordToDatabase, Long> wordDao;
    private int repeatScore;
    private int progressPercent = 20;


    RepeatLogic(WordToDatabase wordToDatabase, int repeatScore) {
        this.wordToDatabase = wordToDatabase;
        this.repeatScore = repeatScore;
    }


    protected void rememberFromCardRepeatActivity(Context ctx) {
        long timeForLastRepeat = System.currentTimeMillis() - wordToDatabase.getLastRepeatDate();

        switch (repeatScore) {
            case 0:

                wordToDatabase.setLastRepeatDate(System.currentTimeMillis());
                wordToDatabase.setNextRepeatDate(System.currentTimeMillis()+6000);
                wordToDatabase.setSeriesNumber(1);
                break;
            case 1:
                wordToDatabase.setNextRepeatDate(timeForLastRepeat + System.currentTimeMillis()+6000);
                wordToDatabase.setLastRepeatDate(System.currentTimeMillis());
                wordToDatabase.setSeriesNumber(wordToDatabase.getSeriesNumber() + 1);
                break;
            case 2:
                wordToDatabase.setNextRepeatDate(timeForLastRepeat / 100 * progressPercent + timeForLastRepeat + System.currentTimeMillis()+6000);
                wordToDatabase.setLastRepeatDate(System.currentTimeMillis());
                wordToDatabase.setSeriesNumber(wordToDatabase.getSeriesNumber() + 1);
                break;
        }

        WordDbQueries wordDbQueries = new WordDbQueries();
        wordDbQueries.updateWordToDbQuery(ctx, wordToDatabase);
        RepeatHistoryDbQueries repeatHistoryDbQueries= new RepeatHistoryDbQueries();
        repeatHistoryDbQueries.addHistoryToDb(ctx,wordToDatabase,wordToDatabase.getNextRepeatDate(),wordToDatabase.getSeriesNumber()-1,repeatScore);
    }
}



































