package com.example.mate.wordapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import android.widget.Toast;

import com.example.mate.wordapp.db.WordDbQueries;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.activity_main)
    DrawerLayout drawerLayout;
    @BindView(R.id.main_activity_Toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab_main_activity)
    FloatingActionButton learnWordFab;
    @BindView(R.id.add_new_word_fab)
    FloatingActionButton addWordFab;
    @BindView(R.id.user_navigation_view)
    NavigationView navigationView;
    @BindView(R.id.recycler_view_list_of_courses)
    RecyclerView recyclerView;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.frameFirstWord)
    FrameLayout firstWordFrame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        onFirstStartGoToStartActivity();
        addFirstWord();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        learnWordFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CardRepeatActivity.class);
                finish();
                startActivity(intent);
            }
        });

        addWordFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), AddWordActivity.class));
            }
        });
    }

    private void addFirstWord() {
        WordDbQueries wordDbQueries = new WordDbQueries();
        if (valueOf(wordDbQueries.countAllWordsInDbQuery(getApplicationContext())).equals("0")) {
            learnWordFab.setVisibility(View.INVISIBLE);
            firstWordFrame.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTextInTextViews();
    }

    private void setTextInTextViews() {
        WordDbQueries wordDbQueries = new WordDbQueries();
        wordDbQueries.coursesFromDb(getApplicationContext());

        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setAdapter(new MainActivityCoursesAdapter(getBaseContext(), wordDbQueries.getWtdList()));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        WordDbQueries wordDbQueries = new WordDbQueries();
        int id = item.getItemId();
        if (valueOf(wordDbQueries.countWordsToRepeatQuery(getApplicationContext())).equals("0") && id == R.id.choose_learning_navigation) {
            Toast.makeText(this, R.string.addWord, Toast.LENGTH_SHORT).show();
        } else {
            NavigationManager navigationManager = new NavigationManager();
            navigationManager.chooseItemNavigationView(getApplicationContext(), id);
            finish();

            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }
    }

    private void onFirstStartGoToStartActivity() {
        PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(getBaseContext());

        if (preferencesLanguageManager.getLanguagePreferencesIso()[0] == preferencesLanguageManager.getLanguagePreferencesIso()[1]) {
            finish();
            startActivity(new Intent(getApplicationContext(), StartActivity.class));
        }
    }

}
