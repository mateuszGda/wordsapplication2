package com.example.mate.wordapp;


import android.os.AsyncTask;

import com.example.mate.wordapp.modelDictionary.DictionaryResponse;
import com.example.mate.wordapp.modelDictionary.TucEntry;
import com.example.mate.wordapp.retrofitConnection.RetrofitApiClient;
import com.example.mate.wordapp.retrofitConnection.RetrofitApiFactory;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class DictionaryAsyncTask extends AsyncTask<String, Void, List<TucEntry>> {

    private final RetrofitApiClient retrofitApiClient;
    private DictionaryCollectionDownloadListener dictionaryCollectionDownloadListener;


    public DictionaryAsyncTask(DictionaryCollectionDownloadListener dictionaryCollectionDownloadListener) {
        RetrofitApiFactory retrofitApiClientFactory = new RetrofitApiFactory();
        retrofitApiClient = retrofitApiClientFactory.create();
        this.dictionaryCollectionDownloadListener = dictionaryCollectionDownloadListener;
    }

    @Override
    protected List<TucEntry> doInBackground(String... params) {

        try {
            return getDictionaryCollections(params[0],params[1],params[2],params[3],params[4] );

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    private List<TucEntry> getDictionaryCollections(String param0, String param1, String param2, String param3, String param4) throws IOException {
        Call<DictionaryResponse> call = retrofitApiClient.getDictionaryCollection(param0,param1,param2,param3,param4);
        Response<DictionaryResponse> response = call.execute();
        if(response.isSuccessful()) {
            DictionaryResponse dictionaryResponse = response.body();
            return dictionaryResponse.getTuc();
        }
        return null;
    }


    @Override
    protected void onPostExecute(List<TucEntry> tucEntries) {
        dictionaryCollectionDownloadListener.onDownloaded(tucEntries);
    }

    public interface DictionaryCollectionDownloadListener {
        void onDownloaded(List<TucEntry> tucEntry);
    }
}
