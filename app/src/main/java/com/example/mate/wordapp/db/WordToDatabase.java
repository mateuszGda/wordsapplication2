package com.example.mate.wordapp.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;






    @DatabaseTable(tableName = "words")
    public class WordToDatabase {



    @DatabaseField (generatedId = true)
    private Long id;

    @DatabaseField
    private String firstWord;
    @DatabaseField
    private String firstWordLanguage;
    @DatabaseField
    private String secondWord;
    @DatabaseField
    private String secondWordLanguage;
    @DatabaseField
    private String firstWordMeaning;
    @DatabaseField
    private String secondWordMeaning;

    @DatabaseField
    private long addDate;
    @DatabaseField
    private long lastRepeatDate;
    @DatabaseField
    private long nextRepeatDate;
    @DatabaseField
    private int seriesNumber;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstWord() {
        return firstWord;
    }

    public void setFirstWord(String firstWord) {
        this.firstWord = firstWord;
    }

    public String getFirstWordLanguage() {
        return firstWordLanguage;
    }

    public void setFirstWordLanguage(String firstWordLanguage) {
        this.firstWordLanguage = firstWordLanguage;
    }

    public String getSecondWord() {
        return secondWord;
    }

    public void setSecondWord(String secondWord) {
        this.secondWord = secondWord;
    }

    public String getSecondWordLanguage() {
        return secondWordLanguage;
    }

    public void setSecondWordLanguage(String secondWordLanguage) {
        this.secondWordLanguage = secondWordLanguage;
    }

    public String getFirstWordMeaning() {
        return firstWordMeaning;
    }

    public void setFirstWordMeaning(String firstWordMeaning) {
        this.firstWordMeaning = firstWordMeaning;
    }

    public String getSecondWordMeaning() {
        return secondWordMeaning;
    }

    public void setSecondWordMeaning(String secondWordMeaning) {
        this.secondWordMeaning = secondWordMeaning;
    }

    public long getAddDate() {
        return addDate;
    }

    public void setAddDate(long addDate) {
        this.addDate = addDate;
    }

    public long getLastRepeatDate() {
        return lastRepeatDate;
    }

    public void setLastRepeatDate(long lastRepeatDate) {
        this.lastRepeatDate = lastRepeatDate;
    }

    public long getNextRepeatDate() {
        return nextRepeatDate;
    }

    public void setNextRepeatDate(long nextRepeatDate) {
        this.nextRepeatDate = nextRepeatDate;
    }

    public int getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(int seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

        @Override
        public String toString() {
            return "WordToDatabase{" +
                    "id=" + id +
                    ", firstWord='" + firstWord + '\'' +
                    ", firstWordLanguage='" + firstWordLanguage + '\'' +
                    ", secondWord='" + secondWord + '\'' +
                    ", secondWordLanguage='" + secondWordLanguage + '\'' +
                    ", firstWordMeaning='" + firstWordMeaning + '\'' +
                    ", secondWordMeaning='" + secondWordMeaning + '\'' +
                    ", addDate=" + addDate +
                    ", lastRepeatDate=" + lastRepeatDate +
                    ", nextRepeatDate=" + nextRepeatDate +
                    ", seriesNumber=" + seriesNumber +
                    '}';
        }
    }
