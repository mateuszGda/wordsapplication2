package com.example.mate.wordapp;

import android.content.Context;
import android.content.Intent;

/**
 * Created by użytkownik on 2017-03-05.
 */

public class NavigationManager {
    public NavigationManager() {

    }

    public void chooseItemNavigationView(Context context, int id) {

        Intent i;

        switch (id) {
            case R.id.dictionary_navigation:
                i = new Intent(context, DictionaryActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;
            case R.id.settings_navigation:
                i = new Intent(context, SettingsActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;
            case R.id.editwords_navigation:
                i = new Intent(context, WordListActivity.class);
                context.startActivity(i);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;
            case R.id.choose_learning_navigation:
                i = new Intent(context, CardRepeatActivity.class);
                PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(context);
                String s = preferencesLanguageManager.getLanguagePreferencesIso()[1];
                i.putExtra("languageToRepeat",s);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;
            case R.id.addword_navigation:
                i = new Intent(context, AddWordActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;
            case R.id.import_export_navigation:
                i = new Intent(context, ImportExportActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;
            case R.id.chart_navigation:
                i = new Intent(context, ChartActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;

        }
    }
}
