package com.example.mate.wordapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.mate.wordapp.charts.ChartEntriesMaker;
import com.github.mikephil.charting.charts.LineChart;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChartWordActivity extends AppCompatActivity {
    @BindView(R.id.word_chart_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_word);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ChartEntriesMaker chartEntriesMaker = new ChartEntriesMaker();

        long id = getIntent().getLongExtra("ID", 0L);
        LineChart chart = (LineChart) findViewById(R.id.line_chart);
        chartEntriesMaker.wordRepeatHistoryEntries(getApplicationContext(), id, chart);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return true;
    }
}