package com.example.mate.wordapp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartActivity extends AppCompatActivity {


    @BindView(R.id.language_spinner_start_activity)
    Spinner spinnerStartLanguage;
    @BindView(R.id.setLanguageButton)
    Button setLanguageButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);


        ArrayAdapter<Language> arrayLanguageAdapter = new ArrayAdapter<Language>(this, R.layout.spinner_white, Language.values());
        arrayLanguageAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerStartLanguage.setAdapter(arrayLanguageAdapter);




        setLanguageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesLanguageManager preferencesLanguageManager = new PreferencesLanguageManager(getApplicationContext());
                String s = Locale.getDefault().getISO3Language();
                preferencesLanguageManager.savePreferences(s, spinnerStartLanguage.getSelectedItem().toString());
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                finish();
            }
        });

    }
}
