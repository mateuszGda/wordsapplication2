package com.example.mate.wordapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mate.wordapp.db.WordDbHelper;
import com.example.mate.wordapp.db.WordDbQueries;
import com.example.mate.wordapp.db.WordToDatabase;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.String.valueOf;

public class WordListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.user_navigation_view)
    NavigationView navigationView;
    @BindView(R.id.word_list_dl)
    DrawerLayout drawerLayout;
    @BindView(R.id.word_list_toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view_list_of_saved_words)
    RecyclerView recyclerView;
    @BindView(R.id.search_editText)
    EditText search;
    @BindView(R.id.search_button)
    ImageButton searchButton;

    private WordDbHelper wordDbHelper;
    private List<WordToDatabase> wordsListFoundInDb;
    private String searchLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_list);
        ButterKnife.bind(this);

        searchLanguage = getIntent().getStringExtra("languageToRepeat");


        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchWords();
                    checkExistence(v);
                    return true;
                }
                return false;
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchWords();
                checkExistence(v);
            }
        });

    }

    private void searchWords() {
        WordDbQueries wordDbQueries = new WordDbQueries();
        if (searchLanguage == null) {
            wordDbQueries.searchWordLike(getApplicationContext(), search.getText().toString());
        }else{
            wordDbQueries.searchWordLikeInLanguage(getApplicationContext(), search.getText().toString(), searchLanguage);
        }

        wordsListFoundInDb = wordDbQueries.getWtdList();

    }

    private void checkExistence(View v){
        if (wordsListFoundInDb.size() == 0) {
            Snackbar.make(v, getResources().getText(R.string.word_doesnt_exist), 2000).show();
        } else {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        setInRecyclerView();
    }

    private void setInRecyclerView() {
        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        //recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        recyclerView.setAdapter(new WordsInDbListAdapter(getBaseContext(), wordsListFoundInDb));
    }


    @Override
    protected void onResume() {
        searchWords();
        setInRecyclerView();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.word_list_dl);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_toolbar, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        WordDbQueries wordDbQueries = new WordDbQueries();
        int id = item.getItemId();
        if (valueOf(wordDbQueries.countWordsToRepeatQuery(getApplicationContext())).equals("0") && id == R.id.choose_learning_navigation) {
            Toast.makeText(this, R.string.addWord, Toast.LENGTH_SHORT).show();
        } else {
            NavigationManager navigationManager = new NavigationManager();
            navigationManager.chooseItemNavigationView(getApplicationContext(), id);
            finish();

            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }
}
